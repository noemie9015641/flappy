using UnityEngine;


public class obstacle : MonoBehaviour
{
    public float speed = 5f;
    public GameObject end;
    // Start is called before the first frame update
    private void Start()
    {
       end = GameObject.FindGameObjectWithTag("mur");
    }

    private void Update()
    {
        transform.position += Vector3.left * speed * Time.deltaTime;
        if (end.transform.position.x > transform.position.x + 7)
        {
            Destroy(gameObject);
        }
    }
}
